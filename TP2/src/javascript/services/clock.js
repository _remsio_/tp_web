'use strict';

StarBus.service('clockService', function(){

    var self = this;


    self.startClock = function () {

        self.today = new Date();

        self.hours = self.today.getHours();
        self.minutes = self.today.getMinutes();
        self.seconds = self.today.getSeconds();
        //Set the AM or PM time

        //Put 0 in front of single digit minutes and seconds

        if (self.minutes<10){
            self.minutes = "0" + self.minutes;
        }

        if (self.seconds<10){
            self.seconds = "0" + self.seconds;
        }
    };


});