'use strict';

StarBus.service('geolocService', function(){

    var self = this;

    self.layerGroup = undefined;

    self.parseBusData = function(data) {

        var result = [];
        data['records'].forEach(function(element) {

            if(element['fields']['etat'] === 'En ligne'){
                result.push([element['geometry']['coordinates'][0], element['geometry']['coordinates'][1], element['fields']['etat']]);
            }

        });
        return result;
    };

    self.getLignes = function(map){


        //localstorage
        if (localStorage.getItem("lignes") !== null){
            console.log("données chargées");
            var retrievedObject = localStorage.getItem('lignes');
            var donnees = JSON.parse(retrievedObject);
            self.addLignes(map, donnees) ;

            //Pas besoin d'appeler le reste de la fonction
            return;
        }

        var xhr = null;

        if(window.XMLHttpRequest)
            xhr = new XMLHttpRequest();
        else if(window.ActiveXObject)
            xhr = new ActiveXObject("Microsoft.XMLHTTP");
        else {
            console.log("XmlHttpRequest non supporté sur ce navigateur");
        }

        xhr.open("GET", "https://data.rennesmetropole.fr/api/records/1.0/search/?dataset=parcours-des-lignes-de-bus-du-reseau-star&sort=idligne&facet=idligne&facet=nomcourtligne&facet=senscommercial&facet=type&facet=nomarretdepart&facet=nomarretarrivee&facet=estaccessiblepmr&refine.senscommercial=Aller&refine.type=Principal", true);

        xhr.send(null);

        xhr.onreadystatechange = function()
        {
            if(xhr.readyState == 4)
            {
                if(xhr.status == 200){

                    var reponse = JSON.parse(xhr.response);
                    console.log(reponse);
                    self.addLignes(map, reponse.records);

                    localStorage.setItem('lignes', JSON.stringify(reponse.records));

                } else {
                    console.log("erreur lors de la requête");
                }

            }
        }
    };

    self.getBus = function(map) {

        var xhr = null;

        if(window.XMLHttpRequest)
            xhr = new XMLHttpRequest();
        else if(window.ActiveXObject)
            xhr = new ActiveXObject("Microsoft.XMLHTTP");
        else {
            console.log("XmlHttpRequest non supporté sur ce navigateur");
        }

        xhr.open("GET", "https://data.rennesmetropole.fr/api/records/1.0/search/?dataset=position-des-bus-en-circulation-sur-le-reseau-star-en-temps-reel&rows=450&facet=numerobus&facet=nomcourtligne&facet=sens&facet=destination", true);

        xhr.send(null);

        xhr.onreadystatechange = function()
        {
            if(xhr.readyState == 4)
            {
                if(xhr.status == 200){

                    var reponse = JSON.parse(xhr.response);
                    console.log(reponse);

                    if(self.layerGroup !== undefined){
                        self.clearPoints(map);
                    }

                    self.addPoints(map, self.parseBusData(reponse));
                } else {
                    console.log("erreur lors de la requête");
                }

            }
        }

    };

    self.addLignes = function (map, lignesList){

        console.log(lignesList);

        lignesList.forEach(function(element){
            L.geoJson(element.fields.parcours, {
                "style": {
                    "color":element.fields.couleurtrace,
                    "stroke-width":"3"
                },
            }).addTo(map);

            L.marker([element.fields.parcours.coordinates[0][1], element.fields.parcours.coordinates[0][0]], {"style": {color:element.fields.couleurtrace}, "title" : "[" + element.fields.nomcourtligne + "] Départ " + element.fields.nomarretdepart}).addTo(map);

            L.marker([element.fields.parcours.coordinates[element.fields.parcours.coordinates.length - 1][1], element.fields.parcours.coordinates[element.fields.parcours.coordinates.length - 1][0]],
                {"style": {color:element.fields.couleurtrace}, "title" : "[" + element.fields.nomcourtligne + "] Terminus " + element.fields.nomarretarrivee
                }).addTo(map);
        });

    };

    self.addPoints = function (map, markerList) {


        self.layerGroup = L.layerGroup().addTo(map);

        // create markers

        markerList.forEach(function(element) {
            if(element !== undefined && element[2] === 'En ligne'){

                var busIcon = L.icon({
                    iconUrl: 'src/img/bus.png',

                    iconSize:     [32, 32], // size of the icon
                    shadowSize:   [40, 40], // size of the shadow
                    iconAnchor:   [32, 32], // point of the icon which will correspond to marker's location
                });
                L.marker([element[1], element[0]], {icon: busIcon}).addTo(self.layerGroup);
            }
        });

    };
    
    self.clearPoints = function (map) {
        self.layerGroup.clearLayers();
    };

});