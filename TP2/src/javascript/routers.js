StarBus.config(['$stateProvider', '$urlRouterProvider', function( $stateProvider, $urlRouterProvider ) {
    $urlRouterProvider.otherwise('/');


    var homeState = {
        name: 'home',
        url: '/',
        templateUrl: './src/templates/home.template.html',
        controller: 'HomeCtrl as home'
    };

    var adminState = {
        name: 'admin',
        url: '/admin',
        templateUrl: './src/templates/admin.template.html',
        controller: 'AdminCtrl as admin'
    };

    $stateProvider.state( homeState );
    $stateProvider.state( adminState );

}]);