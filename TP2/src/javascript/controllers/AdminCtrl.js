'use strict';

StarBus.controller('AdminCtrl', ['$scope', 'geolocService', 'clockService', function ($scope, geolocService, clockService) {
    var self = this;

    self.map = L.map('map').setView([48.1033, -1.6853], 11);
    $scope.hours = clockService.hours;
    $scope.minutes = clockService.minutes;
    $scope.seconds = clockService.seconds;

    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
        maxZoom: 18,
        id: 'mapbox.light'
    }).addTo(self.map);

    $scope.getBus = function(){
        geolocService.getBus(self.map);
    };

    self.updateClock = function(){
        setInterval(function () {
            clockService.startClock();
            $scope.hours = clockService.hours;
            $scope.minutes = clockService.minutes;
            $scope.seconds = clockService.seconds;
            $scope.$apply();
        }, 1000);
    };

    geolocService.getBus(self.map);
    geolocService.getLignes(self.map);
    self.updateClock();





}]);