function setImage(weather, id)
{
    var weatherDiv = document.getElementById(id);
    var url = "http://openweathermap.org/img/w/"+weather.weather[0].icon+".png";
    weatherDiv.src = url;

}

function updateWeather(weatherResult)
{
    var domVilles = document.querySelectorAll(".ville");
    var domTemperatures = document.querySelectorAll(".temperature-j1");
    var domTemperaturesMinimum = document.querySelectorAll(".temperature-min-j1");
    var domTemperaturesMaximum = document.querySelectorAll(".temperature-max-j1");
    var domPression = document.querySelectorAll(".pression-j1");

    Array.prototype.forEach.call(domVilles, function(elements, index) {
        elements.innerHTML = weatherResult["name"];
    });
    Array.prototype.forEach.call(domTemperatures, function(elements, index) {
        elements.innerHTML = "Température : " + weatherResult["main"]["temp"];
    });
    Array.prototype.forEach.call(domTemperaturesMinimum, function(elements, index) {
        elements.innerHTML = "Température minimum: " + weatherResult["main"]["temp_min"];
    });
    Array.prototype.forEach.call(domTemperaturesMaximum, function(elements, index) {
        elements.innerHTML = "Température maximum: " + weatherResult["main"]["temp_max"];
    });
    Array.prototype.forEach.call(domPression, function(elements, index) {
        elements.innerHTML = "Pression : " + weatherResult["main"]["pressure"] + " Pa";
    });

    setImage(weatherResult, "weather-image");

    return weatherResult;
}

/* mise à jour des élements des jours à venir */
function updateWeatherTomorrow(weatherForecastResult)
{

    //
    jour1 = weatherForecastResult['list'][8];
    jour2 = weatherForecastResult['list'][16];
    jour3 = weatherForecastResult['list'][24];

    var domTemperatures = document.querySelectorAll(".temperature-j2");
    var domTemperaturesMinimum = document.querySelectorAll(".temperature-min-j2");
    var domTemperaturesMaximum = document.querySelectorAll(".temperature-max-j2");
    var domPression = document.querySelectorAll(".pression-j2");

    Array.prototype.forEach.call(domTemperatures, function(elements, index) {
        elements.innerHTML = "Température : " + jour1["main"]["temp"];
    });
    Array.prototype.forEach.call(domTemperaturesMinimum, function(elements, index) {
        elements.innerHTML = "Température minimum: " + jour1["main"]["temp_min"];
    });
    Array.prototype.forEach.call(domTemperaturesMaximum, function(elements, index) {
        elements.innerHTML = "Température maximum: " + jour1["main"]["temp_max"];
    });
    Array.prototype.forEach.call(domPression, function(elements, index) {
        elements.innerHTML = "Pression : " + jour1["main"]["pressure"] + " Pa";
    });

    setImage(jour1, "weather-image2");

    domTemperatures = document.querySelectorAll(".temperature-j3");
    domTemperaturesMinimum = document.querySelectorAll(".temperature-min-j3");
    domTemperaturesMaximum = document.querySelectorAll(".temperature-max-j3");
    domPression = document.querySelectorAll(".pression-j3");

    Array.prototype.forEach.call(domTemperatures, function(elements, index) {
        elements.innerHTML = "Température : " + jour2["main"]["temp"];
    });
    Array.prototype.forEach.call(domTemperaturesMinimum, function(elements, index) {
        elements.innerHTML = "Température minimum: " + jour2["main"]["temp_min"];
    });
    Array.prototype.forEach.call(domTemperaturesMaximum, function(elements, index) {
        elements.innerHTML = "Température maximum: " + jour2["main"]["temp_max"];
    });
    Array.prototype.forEach.call(domPression, function(elements, index) {
        elements.innerHTML = "Pression : " + jour2["main"]["pressure"] + " Pa";
    });

    setImage(jour1, "weather-image3");

}

/* Appel vers l'API de l'instant présent */
function callAPI(url)
{
    var xhr = null;

    if(window.XMLHttpRequest)
        xhr = new XMLHttpRequest();
    else if(window.ActiveXObject)
        xhr = new ActiveXObject("Microsoft.XMLHTTP");
    else {
        console.log("XmlHttpRequest non supporté sur ce navigateur");
    }

    xhr.open("GET", url, true);
    xhr.send(null);

    xhr.onreadystatechange = function()
    {
        if(xhr.readyState == 4)
        {
            if(xhr.status == 200){
                var reponse = JSON.parse(xhr.response);
                updateWeather(reponse);
            } else {
                console.log("erreur lors de la requête");
            }

        }
    }
}

/* Appel vers l'API forecast */
function callAPIForecast(url)
{
    var xhr = null;

    if(window.XMLHttpRequest)
        xhr = new XMLHttpRequest();
    else if(window.ActiveXObject)
        xhr = new ActiveXObject("Microsoft.XMLHTTP");
    else {
        console.log("XmlHttpRequest non supporté sur ce navigateur");
    }

    xhr.open("GET", url, true);
    xhr.send(null);

    xhr.onreadystatechange = function()
    {
        if(xhr.readyState == 4)
        {
            if(xhr.status == 200){
                var reponse = JSON.parse(xhr.response);
                updateWeatherTomorrow(reponse);
                console.log(reponse);
            } else {
                console.log("erreur lors de la requête");
            }

        }
    }
}

//ajout de l'evenement onclick

var boutonRecherche = document.getElementById("bouton-recherche");

boutonRecherche.addEventListener("click", function(){

    var recherche = document.getElementById("recherche").value;


    callAPI("http://api.openweathermap.org/data/2.5/weather?q="+ recherche +"&APPID=ee07e2bf337034f905cde0bdedae3db8&units=metric");
    callAPIForecast("http://api.openweathermap.org/data/2.5/forecast?q="+ recherche +"&APPID=ee07e2bf337034f905cde0bdedae3db8&units=metric");
});

window.addEventListener('keypress', function (e) {
    if (e.keyCode == 13) {
        var recherche = document.getElementById("recherche").value;

        callAPI("http://api.openweathermap.org/data/2.5/weather?q="+ recherche +"&APPID=ee07e2bf337034f905cde0bdedae3db8&units=metric");
        callAPIForecast("http://api.openweathermap.org/data/2.5/forecast?q="+ recherche +"&APPID=ee07e2bf337034f905cde0bdedae3db8&units=metric");
    }
}, false);


//initialisation de la ville à Vannes
callAPI("http://api.openweathermap.org/data/2.5/weather?q=vannes,fr&APPID=ee07e2bf337034f905cde0bdedae3db8&units=metric");
callAPIForecast("http://api.openweathermap.org/data/2.5/forecast?q=vannes,fr&APPID=ee07e2bf337034f905cde0bdedae3db8&units=metric");